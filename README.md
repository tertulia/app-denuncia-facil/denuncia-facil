# Denúncia fácil

[Documentação 1.0](https://gitlab.com/tertulia/app-denuncia-facil/denuncia-facil/wikis/Documenta%C3%A7%C3%A3o)

[1. Descrição do Projeto](https://gitlab.com/tertulia/app-denuncia-facil/denuncia-facil/wikis/Documenta%C3%A7%C3%A3o#1-descri%C3%A7%C3%A3o-do-projeto)

[2. Análise](https://gitlab.com/tertulia/app-denuncia-facil/denuncia-facil/wikis/Documenta%C3%A7%C3%A3o#2-an%C3%A1lise)

[2.1 Análise dos Riscos](https://gitlab.com/tertulia/app-denuncia-facil/denuncia-facil/wikis/Documenta%C3%A7%C3%A3o#21-an%C3%A1lise-dos-riscos)

[2.2 Análise dos Requisitos](https://gitlab.com/tertulia/app-denuncia-facil/denuncia-facil/wikis/Documenta%C3%A7%C3%A3o#22-an%C3%A1lise-dos-requisitos)

[2.3 Descrição dos Casos de Uso](https://gitlab.com/tertulia/app-denuncia-facil/denuncia-facil/wikis/Documenta%C3%A7%C3%A3o#23-descri%C3%A7%C3%A3o-dos-casos-de-uso)

[2.3.1 Diagrama de Casos de Uso](https://gitlab.com/tertulia/app-denuncia-facil/denuncia-facil/wikis/Documenta%C3%A7%C3%A3o#231-diagrama-de-casos-de-uso)

[3. Projeto](https://gitlab.com/tertulia/app-denuncia-facil/denuncia-facil/wikis/Documenta%C3%A7%C3%A3o#3-projeto)

[3.1 Definição dos Módulos](https://gitlab.com/tertulia/app-denuncia-facil/denuncia-facil/wikis/Documenta%C3%A7%C3%A3o#31-defini%C3%A7%C3%A3o-dos-m%C3%B3dulos)

[3.1.1 Módulo de Usuário](https://gitlab.com/tertulia/app-denuncia-facil/denuncia-facil/wikis/Documenta%C3%A7%C3%A3o#311-m%C3%B3dulo-de-usu%C3%A1rio)

[3.1.2 Módulo de Ocorrência](https://gitlab.com/tertulia/app-denuncia-facil/denuncia-facil/wikis/Documenta%C3%A7%C3%A3o#312-m%C3%B3dulo-de-ocorr%C3%AAncia)

[3.2 Processo de diversificação e convergência](https://gitlab.com/tertulia/app-denuncia-facil/denuncia-facil/wikis/Documenta%C3%A7%C3%A3o#32-processo-de-diversifica%C3%A7%C3%A3o-e-converg%C3%AAncia)

[3.3 Definição de protótipos das telas](https://gitlab.com/tertulia/app-denuncia-facil/denuncia-facil/wikis/Documenta%C3%A7%C3%A3o#33-defini%C3%A7%C3%A3o-de-prot%C3%B3tipos-das-telas)