import 'package:denuncia_facil/pages/signup_page.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        //key: _formKey,
        child: ListView(
          padding: EdgeInsets.all(16.0),
          children: <Widget>[
            SizedBox(height: 30.0,),
            Center(
              child: Text("Acesse sua conta", style: TextStyle(fontSize: 20.0,),),
            ),
            SizedBox(height: 50.0,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  width: 155.0,
                  height: 44.0,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50.0)
                    ),
                    color: Colors.blueAccent, 
                    onPressed: () {},
                    child: Row(
                      children: <Widget>[
                        Container(width: 40.0, height: 40.0,child: Image.asset("icons/facebook.png"),),
                        SizedBox(width: 10.0,),
                        Text("Facebook")
                      ]
                    ),
                  )
                ),
                SizedBox(width: 10.0,),
                SizedBox(
                  height: 44.0,
                  width: 155.0,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50.0)
                    ),
                    onPressed: () {},
                    child: Row(
                      children: <Widget>[
                        Container(width: 30.0, height: 30.0, child: Image.asset("icons/google.png"),),
                        SizedBox(width: 20.0,),
                        Text("Google")
                      ]
                    ),
                  )
                )
              ],
            ),
            SizedBox(height: 30.0,),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(height: 3.0, color: Colors.black, child: Text("--------------------------------------"),),
                  Text(" ou ", style: TextStyle(fontSize: 15.0,),),
                  Container(height: 3.0, color: Colors.black, child: Text("--------------------------------------"))
                ],
              ), 
            ),
            SizedBox(height: 50.0,),
            Center(
              child: Text("Acesse via e-mail", style: TextStyle(fontSize: 20.0,),),
            ),
            SizedBox(height: 10.0,),
            TextFormField(
              decoration: InputDecoration(
                hintText: "E-mail"
              ),
              keyboardType: TextInputType.emailAddress,
              validator: (text) {
                if(text.isEmpty || !text.contains("@")) return "E-mail inválido!";
              },
            ), 
            SizedBox(height: 16.0,),
            TextFormField(
              decoration: InputDecoration(
                hintText: "Senha"
              ),
              obscureText: true,
              validator: (text) {
                if(text.isEmpty || text.length < 6) return "Senha inválida!";
              },
            ),
            Align(
              alignment: Alignment.centerRight,
              child: FlatButton(
                onPressed: () {

                },
                child: Text("Esqueci minha senha",
                  textAlign: TextAlign.right,
                ),
                padding: EdgeInsets.zero,
              ),
            ),
            SizedBox(height: 16.0),
            SizedBox(
              height: 44.0,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.0)
                ),
                child: Text(
                  "Entrar",
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold
                  ),
                ),
                color: Theme.of(context).primaryColor,
                onPressed: () {
                  /*if(_formKey.currentState.validate()) {
                  
                  }*/
                },
              ),
            ),
            SizedBox(height: 5.0,),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Não possui cadastro ?", style: TextStyle(fontSize: 16.0,),),
                  FlatButton(
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignupPage()));
                    },
                    child: Text("Cadastrar-me", style: TextStyle(fontSize: 16.0, color: Colors.blue)),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );  
  }
}