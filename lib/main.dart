import 'package:flutter/material.dart';
import 'homes/home.dart';

void main() {
  runApp(MaterialApp(
    home: Home(),
    debugShowCheckedModeBanner: false,
    theme: ThemeData(primarySwatch: Colors.amber),
  ));
}

